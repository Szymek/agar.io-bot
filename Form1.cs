﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.WebSockets;
using System.Threading;
using System.Text.RegularExpressions;
using Agar.IO_Bot.Enums;

namespace Agar.IO_Bot
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Console.WriteLine("Loaded");
            this.Map.StartMapDrawingThread();
            Connect();
        }
        private async void Connect()
        {
            Game.Player.Socket = new ClientWebSocket();
            var ip = Regex.Match(new WebClient().DownloadString("http://m.agar.io/findServer"), "ip\":\"(.*?)\"").Groups[1].Value;
            await Game.Player.Socket.ConnectAsync(new Uri($"ws://{ip}/"), CancellationToken.None);
            if (Game.Player.Socket.State != WebSocketState.Open)
            {
                Console.WriteLine("State: {0}", Game.Player.Socket.State);
                return;
            }

            await new Packets.CS.Init1().Send(Game.Player.Socket);
            await new Packets.CS.Init2().Send(Game.Player.Socket);
            await new Packets.CS.Token().Send(Game.Player.Socket);

            Console.WriteLine("Sent login packets");
            this.Map.ConnectionState = ConnectionState.Connected;

            await Task.Factory.StartNew(ReceiveData);
        }
        private async void ReceiveData()
        {
            var buffer = new byte[10000];
            while (Game.Player.Socket.State == WebSocketState.Open)
            {
                var res = await Game.Player.Socket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                if (res.MessageType == WebSocketMessageType.Close)
                    await Game.Player.Socket.CloseAsync(WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
                else if (res.MessageType == WebSocketMessageType.Binary)
                {
                    var pBytes = new byte[res.Count];
                    Array.Copy(buffer, pBytes, pBytes.Length);

                    using (var pr = new Packets.PacketReader(pBytes, false))
                    {
                        var id = pr.ReadUInt8();
                        var packet = Game.GetPacketByID(id);
                        if (packet == null)
                            Console.WriteLine($"Unknown packet {id}");
                        else                        
                            packet.Read(pBytes);                        
                    }
                }
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Game.Player.Respawn("MY_USERNAME");
            button1.Visible = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            button1.Visible = !Game.Player.IsAlive;
        }
    }
}
