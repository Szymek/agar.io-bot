﻿using System;
using System.Text;
using Agar.IO_Bot.Enums;
using Agar.IO_Bot.Helpers;

namespace Agar.IO_Bot
{
    public class Blob
    {
        public uint ID { get; set; }
        public string Username
        {
            get
            {
                if (Type == ObjectType.Virus)
                    return "VIRUS";
                else if (string.IsNullOrEmpty(_nick))
                    return "NO_NAME";
                return _nick;
            }
            set
            {
                _nick = value;
            }
        }
        public Position Position { get; set; }
        public ushort Size { get; set; }
        public RGB Color { get; set; }
        public ObjectType Type { get; set; }
        public string Skin { get; set; }
        public DrawingInfo DrawingInfo { get; set; }
        public int Mass => Convert.ToInt32(Math.Pow(Size, 2) / 100);

        public Blob()
        {
            Position = new Position(0, 0);
            Color = new RGB();
            DrawingInfo = new DrawingInfo(this);
        }
        public void Eaten(Blob eater)
        {
            Game.Blobs.Remove(this);
        }
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("----------------------------------BLOB--------------------");
            sb.AppendLine("ID: " + ID);
            sb.AppendLine("Username: " + Username);
            sb.AppendLine("Position: " + Position);
            sb.AppendLine("Size: " + Size);
            sb.AppendLine("Color: " + Color);
            sb.AppendLine("Type: " + Type);
            sb.AppendLine("Skin:" + Skin);
            return sb.ToString();
        }

        private string _nick;
    }
}
