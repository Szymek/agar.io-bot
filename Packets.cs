﻿using System;
using System.Collections;
using System.Linq;
using System.Net;
using System.Net.WebSockets;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Agar.IO_Bot.Enums;
using Agar.IO_Bot.Helpers;

namespace Agar.IO_Bot
{

    namespace Packets
    {
        public interface IPacket
        {
            byte ID { get; }
        }
        public interface ICsPacket : IPacket
        {
            byte[] Write();
            byte[] Packet { get; set; }
            Task Send(ClientWebSocket socket);
        }
        public interface IScPacket : IPacket
        {
            void Read(byte[] buffer);
        }
        namespace CS
        {
            public class Init1 : ICsPacket
            {
                public byte ID => 254;

                private byte[] _packet;
                public byte[] Packet
                {
                    get
                    {
                        if (_packet == null)
                            Write();
                        return _packet;
                    }
                    set
                    {
                        this._packet = value;
                    }
                }

                public byte[] Write()
                {
                    using (var pw = new PacketWriter(ID))
                    {
                        pw.WriteUInt32(5);
                        Packet = pw.GetPacket();
                    }
                    return Packet;
                }

                public async Task Send(ClientWebSocket socket)
                {
                    await socket.SendAsync(new ArraySegment<byte>(Packet), WebSocketMessageType.Binary, true, CancellationToken.None);
                }
            }
            public class Init2 : ICsPacket
            {
                public byte ID => 255;

                private byte[] _packet;
                public byte[] Packet
                {
                    get
                    {
                        if (_packet == null)
                            Write();
                        return _packet;
                    }
                    set
                    {
                        this._packet = value;
                    }
                }

                public byte[] Write()
                {
                    using (var pw = new PacketWriter(ID))
                    {
                        pw.WriteUInt32(154669603);
                        Packet = pw.GetPacket();
                    }
                    return Packet;
                }

                public async Task Send(ClientWebSocket socket)
                {
                    await socket.SendAsync(new ArraySegment<byte>(Packet), WebSocketMessageType.Binary, true, CancellationToken.None);
                }
            }
            public class Token : ICsPacket
            {
                public byte ID => 80;

                private byte[] _packet;
                public byte[] Packet
                {
                    get
                    {
                        if (_packet == null)
                            Write();
                        return _packet;
                    }
                    set
                    {
                        this._packet = value;
                    }
                }

                public byte[] Write()
                {
                    using (var pw = new PacketWriter(ID))
                    {
                        pw.WriteNullStr8(GetToken());
                        Packet = pw.GetPacket();
                    }
                    return Packet;
                }

                public async Task Send(ClientWebSocket socket)
                {
                    await socket.SendAsync(new ArraySegment<byte>(Packet), WebSocketMessageType.Binary, true, CancellationToken.None);
                }
                private static string GetToken()
                {
                    var token = Regex.Match(new WebClient().DownloadString("http://m.agar.io/findServer"), "token\":\"(.*?)\"").Groups[1].Value;
                    Console.WriteLine("Token: {0}", token);
                    return token;
                }
            }
            public class Spawn : ICsPacket
            {
                public byte ID => 0;

                private byte[] _packet;
                public byte[] Packet
                {
                    get
                    {
                        if (_packet == null)
                            Write();
                        return _packet;
                    }
                    set
                    {
                        this._packet = value;
                    }
                }

                public byte[] Write()
                {
                    using (var pw = new PacketWriter(ID))
                    {
                        pw.WriteEndStr16(Game.Player.Username);
                        Packet = pw.GetPacket();
                    }
                    return Packet;
                }

                public async Task Send(ClientWebSocket socket)
                {
                    await socket.SendAsync(new ArraySegment<byte>(Packet), WebSocketMessageType.Binary, true, CancellationToken.None);
                }
            }
            public class Move : ICsPacket
            {
                public byte ID => 16;
                private int ToX { get; set; }
                private int ToY { get; set; }
                public Move(int tox, int toy)
                {
                    this.ToX = tox;
                    this.ToY = toy;
                }

                private byte[] _packet;
                public byte[] Packet
                {
                    get
                    {
                        if (_packet == null)
                            Write();
                        return _packet;
                    }
                    set
                    {
                        this._packet = value;
                    }
                }

                public byte[] Write()
                {
                    using (var pw = new PacketWriter(ID))
                    {
                        pw.WriteInt32(ToX);
                        pw.WriteInt32(ToY);
                        pw.WriteUInt32(0);
                        Packet = pw.GetPacket();
                    }
                    return Packet;
                }

                public async Task Send(ClientWebSocket socket)
                {
                    await socket.SendAsync(new ArraySegment<byte>(Packet), WebSocketMessageType.Binary, true, CancellationToken.None);
                }
            }
        }
        namespace SC
        {
            //sent while moving to fool bots
            public class GameArea : IScPacket
            {
                public byte ID => 64;

                public void Read(byte[] buffer)
                {
                    using (var pr = new PacketReader(buffer))
                    { 
                        var x1 = pr.ReadFloat64();
                        var y1 = pr.ReadFloat64();
                        var x2 = pr.ReadFloat64();
                        var y2 = pr.ReadFloat64();
                        Game.Map.MinX = Math.Min(Game.Map.MinX, Math.Min(x1, x2));
                        Game.Map.MinY = Math.Min(Game.Map.MinY, Math.Min(y1, y2));
                        Game.Map.MaxX = Math.Max(Game.Map.MaxX, Math.Max(x1, x2));
                        Game.Map.MaxY = Math.Max(Game.Map.MaxY, Math.Max(y1, y2));
                        if (buffer.Length > 33)
                        {
                            Game.GameType = (GameType)pr.ReadUInt32();
                            Game.ServerVersion = pr.ReadNullStr16();
                        }
                    }
                }
            }
            public class Leaderboard : IScPacket
            {
                public byte ID => 49;

                public void Read(byte[] buffer)
                {
                    Game.TopPlayers.Clear();
                    using (var pr = new PacketReader(buffer))
                    {
                        var topPlayersCount = pr.ReadUInt32();;

                        for (var i = 0; i < topPlayersCount; i++)                        
                            Game.TopPlayers.Add(new TopPlayersEntry(pr.ReadUInt32() != 0, pr.ReadNullStr16()));                        
                    }
                }
            }
            public class WorldUpdate : IScPacket
            {
                public byte ID => 16;

                public void Read(byte[] buffer)
                {
                    using (var pr = new PacketReader(buffer))
                    {
                        var nBlobEvents = pr.ReadUInt16();
                        for (var i = 0; i < nBlobEvents; i++)
                        {
                            var eater = pr.ReadUInt32();
                            var victim = pr.ReadUInt32();
                            var blob = Game.Blobs.FirstOrDefault(x => x.ID == victim);
                            if (blob != null)
                                blob.Eaten(Game.Blobs.FirstOrDefault(x => x.ID == eater) ?? new Blob() { ID = eater });
                            else if (victim == Game.Player.ID)
                            {
                                Console.WriteLine("RIP");
                                Game.Player.IsAlive = false;
                            }
                        }

                        //updates
                        uint playerID;
                        while ((playerID = pr.ReadUInt32()) != 0)
                        {
                            var x = pr.ReadInt32();
                            var y = pr.ReadInt32();
                            var size = pr.ReadUInt16();
                            var color = new RGB(pr.ReadUInt8(), pr.ReadUInt8(), pr.ReadUInt8());
                            var flags = new BitArray(new byte[] { pr.ReadByte() });
                            var isVirus = flags.Get(0);
                            var shouldReadSkip4 = flags.Get(1);
                            var hasSkin = flags.Get(2);
                            var skin = string.Empty;
                            var username = string.Empty;

                            if (shouldReadSkip4)
                            {
                                var skipCount = pr.ReadUInt32();
                                pr.MemoryStream.Position += skipCount + 4;
                            }

                            if (hasSkin)
                                skin = pr.ReadNullStr8();
                            username = pr.ReadNullStr16();


                            if (x > Game.Map.Width * 2 || y > Game.Map.Height * 2) //?
                                continue;

                            Blob blob;
                            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(Game.Player.Username) && username.Equals(Game.Player.Username))
                            {
                                Console.WriteLine($"player's blob => {playerID}");
                                Game.Player.ID = playerID;
                                Game.Player.IsAlive = true;
                                blob = Game.Player.Blob;
                                blob.Type = ObjectType.OwnBlob;
                            }
                            else
                            {
                                blob = Game.Blobs.FirstOrDefault(b => b.ID == playerID);
                                if (blob == null)
                                {
                                    blob = new Blob();
                                    Game.Blobs.Add(blob);
                                }
                            }

                            blob.ID = playerID;
                            if (string.IsNullOrEmpty(blob.Username))
                                blob.Username = username;
                            blob.Position.X = x;
                            blob.Position.Y = y;
                            blob.Size = size;
                            blob.Color = color;
                            if (isVirus)
                                blob.Type = ObjectType.Virus;
                            else if (blob.Type != ObjectType.OwnBlob)
                                blob.Type = ObjectType.Player;
                            blob.Skin = skin;
                            if (x < Math.Min(Game.Map.MinX, Game.Map.MaxX) || y < Math.Min(Game.Map.MinY, Game.Map.MaxY) || x > Math.Max(Game.Map.MaxX, Game.Map.MinX) || y > Math.Max(Game.Map.MinY, Game.Map.MaxY))
                                Console.WriteLine($"BORDER ---- {x}/{y}\n{Game.ToString()}");
                            
                        }                        
                        var nBlobRemovals = pr.ReadUInt32();
                        for (var i = 0; i < nBlobRemovals; i++)
                        {
                            var id = pr.ReadUInt32();
                            Game.Blobs.Remove(Game.Blobs.FirstOrDefault(x => x.ID == id));
                        }
                    }
                }
            }
            public class ResetCells : IScPacket
            {
                public byte ID => 18;

                public void Read(byte[] buffer)
                {
                    Game.Blobs.Clear();
                }
            }
            public class OwnedBlob : IScPacket
            {
                public byte ID => 32;

                public void Read(byte[] buffer)
                {
                    using (var pr = new PacketReader(buffer))
                    {
                        var id = pr.ReadUInt32();
                        var obj = Game.Blobs.FirstOrDefault(x => x.ID == id);
                        if (obj != null)
                            obj.Type = ObjectType.OwnBlob;
                    }
                }
            }
        }
    }
}
