﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using Agar.IO_Bot.Enums;
using Agar.IO_Bot.Helpers;

namespace Agar.IO_Bot
{
    public static class Game
    {
        public static string ServerVersion;
        public static GameType GameType;
        public static List<TopPlayersEntry> TopPlayers = new List<TopPlayersEntry>();
        public static List<Blob> Blobs = new List<Blob>();

        public static List<Packets.IScPacket> ServerPackets = new List<Packets.IScPacket>()
        {
            new Packets.SC.GameArea(),
            new Packets.SC.Leaderboard(),
            new Packets.SC.WorldUpdate(),
            new Packets.SC.ResetCells(),
            new Packets.SC.OwnedBlob()
        };
        public static Packets.IScPacket GetPacketByID(int id) => ServerPackets.FirstOrDefault(x => x.ID == id);

        public static class Player
        {
            public static string Username { get; set; }
            public static uint ID { get; set; }
            public static bool IsAlive { get; set; }
            public static Position MoveDirection { get; set; }
            public static Blob Blob = new Blob();
            public static ClientWebSocket Socket;

            public static async void Respawn(string username = "")
            {
                if (!string.IsNullOrEmpty(username))
                    Username = username;
                MoveDirection = null;

                await new Packets.CS.Spawn().Send(Socket);
            }

            public static async void Move(int x, int y)
            {
                MoveDirection = new Position(x, y);
                await new Packets.CS.Move(x, y).Send(Socket);
            }
        }

        public static class Map
        {
            public static double MinX { get; set; }
            public static double MaxX { get; set; }
            public static double MinY { get; set; }
            public static double MaxY { get; set; }
            public static double Width
            {
                get
                {
                    if (width == 0)
                        width = Math.Abs(MaxX) + Math.Abs(MinX);
                    return width;
                }
            }
            public static double Height
            {
                get
                {
                    if (height == 0)
                        height = Math.Abs(MaxY) + Math.Abs(MinY);
                    return height;
                }
            }

            public new static string ToString()
            {
                var sb = new StringBuilder();
                sb.AppendLine("MinX: " + MinX);
                sb.AppendLine("MinY: " + MinY);
                sb.AppendLine("MaxX: " + MaxX);
                sb.AppendLine("MaxY: " + MaxY);
                sb.AppendLine("Height: " + Height);
                sb.AppendLine("Width: " + Width);
                return sb.ToString();
            }
            private static double width, height;
        }


        public new static string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine("-------------------- GAME ---------------------");
            sb.AppendLine("Version: " + ServerVersion);
            sb.AppendLine("Type: " + GameType);

            sb.AppendLine("------------- MAP -----------");
            sb.AppendLine(Map.ToString());

            sb.AppendLine("------------- TOP PLAYERS -----------");
            for (var i = 0; i < TopPlayers.Count; i++)
                sb.AppendLine((i + 1) + ". " + TopPlayers[i].Username + (TopPlayers[i].ShouldHighlight ? "[*]" : ""));
            
            return sb.ToString();
        }
    }
}
