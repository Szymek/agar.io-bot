﻿namespace Agar.IO_Bot
{
    partial class MapUI
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.drawArea = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.drawArea)).BeginInit();
            this.SuspendLayout();
            // 
            // drawArea
            // 
            this.drawArea.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.drawArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.drawArea.Location = new System.Drawing.Point(0, 0);
            this.drawArea.Name = "drawArea";
            this.drawArea.Size = new System.Drawing.Size(400, 400);
            this.drawArea.TabIndex = 1;
            this.drawArea.TabStop = false;
            this.drawArea.SizeChanged += new System.EventHandler(this.map_Resized);
            this.drawArea.Click += new System.EventHandler(this.map_Click);
            this.drawArea.Paint += new System.Windows.Forms.PaintEventHandler(this.map_Paint);
            this.drawArea.MouseMove += new System.Windows.Forms.MouseEventHandler(this.map_MouseMove);
            // 
            // MapUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.drawArea);
            this.Name = "MapUI";
            this.Size = new System.Drawing.Size(400, 400);
            ((System.ComponentModel.ISupportInitialize)(this.drawArea)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox drawArea;
    }
}
