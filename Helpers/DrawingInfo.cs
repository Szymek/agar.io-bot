﻿using System;
using Agar.IO_Bot.Enums;

namespace Agar.IO_Bot.Helpers
{
    public class DrawingInfo
    {
        public bool ShouldDrawUsername { get; set; }
        public float X => (float)((blob.Position.X + Math.Abs(Game.Map.MinX)) / MapUI.ByX);
        public float Y => (float)((blob.Position.Y + Math.Abs(Game.Map.MinY)) / MapUI.ByY);
        public float Size => (float)(this.blob.Size / MapUI.ByX);
        public System.Drawing.Brush Color => this.blob.Type == ObjectType.Virus ? 
            System.Drawing.Brushes.LightGreen : 
            new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(this.blob.Color.R, this.blob.Color.G, this.blob.Color.B));

        private readonly Blob blob;

        public DrawingInfo(Blob b)
        {
            this.blob = b;
        }
    }
}
