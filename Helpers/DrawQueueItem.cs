﻿using System;

namespace Agar.IO_Bot.Helpers
{
    public class DrawQueueItem
    {
        public string Text { get; set; }
        public DateTime TimeAdded { get; set; }
        public DrawQueueItem(string str)
        {
            this.Text = str;
            this.TimeAdded = DateTime.Now;
        }
    }
}
