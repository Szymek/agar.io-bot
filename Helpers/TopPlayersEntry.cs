﻿namespace Agar.IO_Bot.Helpers
{
    public class TopPlayersEntry
    {
        public bool ShouldHighlight { get; set; }
        public string Username { get; set; }

        public TopPlayersEntry(bool p, string n)
        {
            this.Username = n;
            this.ShouldHighlight = p;
        }
    }
}
