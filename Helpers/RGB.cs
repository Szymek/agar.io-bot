﻿namespace Agar.IO_Bot.Helpers
{
    public class RGB
    {
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }
        public RGB()
        {

        }
        public RGB(int r, int g, int b)
        {
            this.R = r;
            this.G = g;
            this.B = b;
        }
        public override string ToString()
        {
            return $"{R}, {G}, {B}";
        }
    }
}
