﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using Agar.IO_Bot.Enums;
using Agar.IO_Bot.Helpers;

namespace Agar.IO_Bot
{
    public partial class MapUI : UserControl
    {
        private readonly Font mapFont;
        private readonly List<DrawQueueItem> drawQueue;
        private static int pbX = 1, pbY = 1;


        public static double ByX => Game.Map.Width / pbX;
        public static double ByY => Game.Map.Height / pbY;
        public ConnectionState ConnectionState { get; set; }

        public MapUI()
        {
            InitializeComponent();

            mapFont = new Font("Segoe UI", 8);
            drawQueue = new List<DrawQueueItem>();
            ConnectionState = ConnectionState.Connecting;
        }
        public void StartMapDrawingThread() //vs designer calls the constructor
        {
            Task.Factory.StartNew(DrawThread);
        }
        public void DrawThread()
        {
            while (ConnectionState != ConnectionState.Disconnected)
            {
                if (drawArea.InvokeRequired)
                    drawArea.Invoke(new __PictureBoxInvokeDelegate(_redraw));
                else
                    _redraw();
                Task.Delay(5);
            }
        }

        private void map_Paint(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;

            if (ConnectionState == ConnectionState.Connecting)
                g.DrawString("Connecting to the server...", mapFont, Brushes.White, 10, 10);
            else if (ConnectionState == ConnectionState.Connected)
            {
                DrawTopPlayers(g);
                DrawBlobs(g);
                if (Game.Player.IsAlive)
                {
                    //direction
                    if (Game.Player.MoveDirection != null)
                    {
                        var dir = Utils.PosToMap(Game.Player.MoveDirection);
                        var pen = new Pen(Color.FromArgb(255, 119, 132, 210));
                        g.DrawLine(pen, Game.Player.Blob.DrawingInfo.X,
                            Game.Player.Blob.DrawingInfo.Y, 
                            dir.X, 
                            dir.Y);
                        g.DrawEllipse(pen, dir.X - 7.5f, dir.Y - 7.5f, 15, 15);
                    }

                    //blob info on the left
                    g.DrawString("Alive", mapFont, Brushes.LightGreen, 10, 10);
                    g.DrawString($"Mass: {Game.Player.Blob.Mass}", mapFont, Brushes.White, 10, 25);

                    //player                   
                    var pBlob = Game.Player.Blob;
                    g.FillEllipse(Brushes.Red,
                        pBlob.DrawingInfo.X - pBlob.DrawingInfo.Size,
                        pBlob.DrawingInfo.Y - pBlob.DrawingInfo.Size,
                        pBlob.DrawingInfo.Size * 2,
                        pBlob.DrawingInfo.Size * 2);
                }
                else
                    g.DrawString("Dead", mapFont, Brushes.Red, 10, 10);
            }

            //messages in the queue
            if (drawQueue.Count > 0)
            {
                var y = 40;
                for (var i = 0; i < drawQueue.Count; i++)
                {
                    if ((DateTime.Now - drawQueue[i].TimeAdded).Seconds >= 4)
                    {
                        drawQueue.RemoveAt(i);
                        continue;
                    }
                    g.DrawString(drawQueue[i].Text, mapFont, Brushes.White, 10, y);
                    y += 15;
                }
            }
        }

        private void DrawObject(Graphics g, Blob blob)
        {
            g.FillEllipse(blob.DrawingInfo.Color, 
                blob.DrawingInfo.X - blob.DrawingInfo.Size, 
                blob.DrawingInfo.Y - blob.DrawingInfo.Size, 
                blob.DrawingInfo.Size *2, 
                blob.DrawingInfo.Size * 2);
            if (!blob.DrawingInfo.ShouldDrawUsername) return;

            var txtSize = g.MeasureString(blob.Username, mapFont);
            g.DrawString(blob.Username, mapFont, Brushes.White, blob.DrawingInfo.X - txtSize.Width / 2, blob.DrawingInfo.Y - txtSize.Height / 2);
        }

        private void DrawTopPlayers(Graphics g)
        {
            float y = 5;
            float biggestWidth = 0;
            

            for (var i = 0; i < Game.TopPlayers.Count; i++)
            {
                if (Game.TopPlayers[i] == null)
                    continue;

                var w = g.MeasureString(Game.TopPlayers[i].Username, mapFont);
                if (w.Width > biggestWidth)
                    biggestWidth = w.Width;
            }

            for (var i = 0; i < Game.TopPlayers.Count; i++)
            {
                if (Game.TopPlayers[i] == null)
                    continue;

                var player = Game.TopPlayers[i];
                g.DrawString($"{i + 1}. {player.Username}", mapFont, 
                    player.ShouldHighlight ? Brushes.Yellow : Brushes.White, this.drawArea.Width - biggestWidth - 15, y);

                y += 15;
            }
        }
        private void DrawBlobs(Graphics g)
        {
            for (var i = 0; i < Game.Blobs.Count; i++)
            {
                var blob = Game.Blobs[i];
                DrawObject(g, blob);
            }
        }
        private void map_MouseMove(object sender, MouseEventArgs e)
        {
            for (var i = 0; i < Game.Blobs.Count; i++)
            {
                var blob = Game.Blobs[i];
                if (blob == null)
                    continue;

                blob.DrawingInfo.ShouldDrawUsername = false;
                if (Math.Pow(e.X - blob.DrawingInfo.X, 2) + Math.Pow(e.Y - blob.DrawingInfo.Y, 2) <= Math.Pow(blob.DrawingInfo.Size, 2))
                    blob.DrawingInfo.ShouldDrawUsername = true;
            }
        }

        private void map_Resized(object sender, EventArgs e)
        {
            pbX = this.drawArea.Width;
            pbY = this.drawArea.Height;
        }

        private void map_Click(object sender, EventArgs ea)
        {
            var e = ea as MouseEventArgs;
            if (e == null) return;

            if (e.Button == MouseButtons.Right)
                Game.Player.MoveDirection = null;
            else if (Game.Player.IsAlive)
            {
                var x = Convert.ToInt32((e.Location.X * ByX) - Math.Abs(Math.Min(Game.Map.MinX, Game.Map.MaxX)));
                var y = Convert.ToInt32((e.Location.Y * ByY) - Math.Abs(Math.Min(Game.Map.MinY, Game.Map.MaxY)));
                Game.Player.Move(x, y);
            }
        }



        private delegate void __PictureBoxInvokeDelegate();
        private void _redraw()
        {
            drawArea.Refresh();
        }
    }
}
