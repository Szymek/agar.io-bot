﻿namespace Agar.IO_Bot.Enums
{
    public enum GameType
    {
        FFA = 0,
        Teams = 1,
        Experimental = 4,
        Party = 8
    }
}
