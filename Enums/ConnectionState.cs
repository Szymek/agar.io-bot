﻿namespace Agar.IO_Bot.Enums
{
    public enum ConnectionState
    {
        Connecting,
        Connected,
        Disconnected
    }
}
