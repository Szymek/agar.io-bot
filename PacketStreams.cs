﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Agar.IO_Bot
{
    namespace Packets
    {
        public class PacketWriter : IDisposable
        {
            public MemoryStream MemoryStream;
            private readonly BinaryWriter binaryWriter;

            public PacketWriter()
            {
                this.MemoryStream = new MemoryStream();
                this.binaryWriter = new BinaryWriter(MemoryStream);
            }
            public PacketWriter(byte id) : this()
            {
                WriteUInt8(id);
            }
            public void WriteUInt8(object val)
            {
                this.binaryWriter.Write((byte)val);
            }
            public void WriteUInt16(object val)
            {
                this.binaryWriter.Write((short)val);
            }
            public void WriteUInt32(object val)
            {
                this.binaryWriter.Write((int)val);
            }
            public void WriteInt32(object val)
            {
                this.binaryWriter.Write((int)val);
            }
            public void WriteFloat32(object val)
            {
                this.binaryWriter.Write((float)val);
            }
            public void WriteFloat64(object val)
            {
                this.binaryWriter.Write((double)val);
            }
            public void WriteNullStr8(object val)
            {
                var str = val.ToString();
                if (!str.EndsWith("\0"))
                    str += "\0";
                this.binaryWriter.Write(Encoding.UTF8.GetBytes(str));
            }
            public void WriteEndStr8(object val)
            {
                this.binaryWriter.Write(Encoding.UTF8.GetBytes(val.ToString()));
            }
            public void WriteEndStr16(object val)
            {
                var str = val.ToString();
                var buffer = Encoding.Unicode.GetBytes(str);
                this.binaryWriter.Write(buffer);
            }
            public void WriteBoolean(bool val)
            {
                this.binaryWriter.Write((byte)(val ? 1 : 0));
            }
            public byte[] GetPacket()
            {
                return this.MemoryStream.ToArray();
            }
            public void Dispose()
            {
                this.binaryWriter.Dispose();
                this.MemoryStream.Dispose();
            }
        }
        public class PacketReader : IDisposable
        {
            public MemoryStream MemoryStream;
            private readonly BinaryReader binaryReader;
            public PacketReader(byte[] buffer, bool offset = true)
            {
                this.MemoryStream = new MemoryStream(buffer);
                this.binaryReader = new BinaryReader(this.MemoryStream);
                if (offset)
                    this.binaryReader.ReadByte();
            }
            public int ReadUInt8()
            {
                return this.binaryReader.ReadByte() & 0xff;
            }
            public ushort ReadUInt16()
            {
                return (ushort)this.binaryReader.ReadUInt16();
            }
            public uint ReadUInt32()
            {
                return this.binaryReader.ReadUInt32();
            }
            public int ReadInt32()
            {
                return this.binaryReader.ReadInt32();
            }
            public float ReadFloat32()
            {
                return this.binaryReader.ReadSingle();
            }
            public double ReadFloat64()
            {
                return this.binaryReader.ReadDouble();
            }
            public string ReadString()
            {
                return this.binaryReader.ReadString().Replace("\0", "");
            }
            public string ReadNullStr16()
            {
                var bytes = new List<byte>();
                for (var i = this.MemoryStream.Position; i < this.MemoryStream.Length; i += 2)
                {
                    var fByte = this.binaryReader.ReadByte();
                    var sByte = this.binaryReader.ReadByte();
                    bytes.Add(fByte);
                    bytes.Add(sByte);
                    if (fByte == 0 && sByte == 0)
                        break;
                }
                return Encoding.Unicode.GetString(bytes.ToArray()).Replace("\0", "");
            }
            public string ReadEndStr16()
            {
                var bytes = new List<byte>();
                while (this.MemoryStream.Position < this.MemoryStream.Length)
                    bytes.Add(this.binaryReader.ReadByte());
                return Encoding.Unicode.GetString(bytes.ToArray());
            }
            public string ReadNullStr8()
            {
                var bytes = new List<byte>();
                byte cByte;
                while ((cByte = this.binaryReader.ReadByte()) != 0)
                    bytes.Add(cByte);
                return Encoding.UTF8.GetString(bytes.ToArray());
            }
            public bool ReadBool()
            {
                return this.binaryReader.ReadByte() == 1;
            }
            public byte ReadByte()
            {
                return this.binaryReader.ReadByte();
            }
            public void Dispose()
            {
                this.binaryReader.Dispose();
                this.MemoryStream.Dispose();
            }
        }
    }
}
