﻿using System;
using System.Linq;
using Agar.IO_Bot.Helpers;

namespace Agar.IO_Bot
{
    public static class Utils
    {
        public static string ByteArrayToHex(byte[] bytes)
        {
            return BitConverter.ToString(bytes).Replace("-", string.Empty);
        }
        public static byte[] HexToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                     .Where(x => x % 2 == 0)
                     .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                     .ToArray();
        }
        public static float PosToMap(double val, bool horizontal = true)
        {
            return (float)((val - (horizontal ? Game.Map.MinX : Game.Map.MinY)) / (horizontal ? MapUI.ByX : MapUI.ByY));
        }
        public static Position PosToMap(Position pos)
        {
            return new Position((int)PosToMap(pos.X), (int)PosToMap(pos.Y, false));
        }
    }
}
